# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
Alarm Application:

1. Ability to set and remove multiple alarms for the day.
2. Ability to set recurring alarms for multiple days.
3. A notification when the alarm is due. 
4. The alarms should be preserved on page reload.
5. Use ReactJS/Angular for front end and MongoDB/MySQL. 



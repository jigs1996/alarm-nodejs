// require modules

var express = require('express');
var app = express();
var body = require('body-parser');
require("./lib/setAlarm.js");
// parse application/x-www-form-urlencoded 
app.use(body.urlencoded({ extended: false })) 
// parse application/json 
app.use(body.json())
//server static dir
app.use(express.static(__dirname+'/html'));
/*
app.get('/',function(req,res){
	res.sendFile('index.html');
});*/

app.get('/test.html',function(req,res){
	res.sendFile('test.html');
});

app.post('/',function(req,res){
	var hh = req.body.hh;
	var mm = req.body.mm;
	var ss = req.body.ss;
	var mode = req.body.mode;
	var day = req.body.day;
	alarm(hh,mm,ss,mode);
	//console.log("Alram: "+req.body.hh+':'+req.body.mm+':'+req.body.ss+'  '+req.body.mode+'  '+req.body.day);
});

var server = app.listen(4444,function(){
	var host = server.address().address;
	var port = server.address().port;
	console.log("Server run on http://localhost:" + port)
});
